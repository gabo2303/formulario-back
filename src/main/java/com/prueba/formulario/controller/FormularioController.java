package com.prueba.formulario.controller;

import java.util.List;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.formulario.domain.Formulario;
import com.prueba.formulario.service.FormularioService;

@RestController
@EnableAutoConfiguration
@RequestMapping("/api")
public class FormularioController {
	
	private final FormularioService formularioService;

    public FormularioController(FormularioService formularioService) {
        this.formularioService = formularioService;
    }
	
	@PostMapping("/ingresaFormulario")
	public ResponseEntity<Formulario> ingresaFormulario(@RequestBody Formulario formulario) 
	{	
		HttpStatus status = null;
		Formulario result = null;

		if(formularioService.findAllByMail(formulario.getMail()).isEmpty()) {			
			result = formularioService.save(formulario);
			status = HttpStatus.CREATED;
		}
		else { status = HttpStatus.BAD_REQUEST; }
        
		return new ResponseEntity<>(result, status);
	}
	
	@GetMapping("/traeFormulariosIngresados")
	public ResponseEntity<List<Formulario>> traeFormulariosIngresados() 
	{	
		List<Formulario> formularios = formularioService.findAll();
        
		return new ResponseEntity<>(formularios, HttpStatus.OK);
	}
}