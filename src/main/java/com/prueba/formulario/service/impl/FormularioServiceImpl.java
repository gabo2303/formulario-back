package com.prueba.formulario.service.impl;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prueba.formulario.domain.Formulario;
import com.prueba.formulario.repository.FormularioRepository;
import com.prueba.formulario.service.FormularioService;

@Service
@Transactional
public class FormularioServiceImpl implements FormularioService {
	
	private final FormularioRepository formularioRepository;
		
    public FormularioServiceImpl(FormularioRepository formularioRepository) 
	{ 
		this.formularioRepository = formularioRepository; 
	}
    
    /**
     * Save a formulario.
     *
     * @param formulrio
     * 		the entity to save
     */
    @Override
    public Formulario save(Formulario formulario) {        
        return formularioRepository.save(formulario);
    }
    
    /**
     * Get all the formularios.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Formulario> findAll() {
        return formularioRepository.findAll();
    }
    
    /**
     * Get all the formularios.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Formulario> findAllByMail(String mail) {
        return formularioRepository.findAllByMail(mail);
    }
}
