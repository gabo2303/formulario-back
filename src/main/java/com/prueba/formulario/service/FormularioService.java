package com.prueba.formulario.service;

import java.util.List;	

import com.prueba.formulario.domain.Formulario;

public interface FormularioService {
	
	/**
     * Guarda Formulario.
     *
     * @param formulario
     *
     * @return la entiedad de persistencia
     */
	Formulario save(Formulario formulario);
	
	/**
     * Obtiene todos los formularios ingresados
     */
	List<Formulario> findAll();
	
	/**
     * Obtiene todos los formularios ingresados con el mail que se intenta ingresar
     */
	List<Formulario> findAllByMail(String mail);
}
