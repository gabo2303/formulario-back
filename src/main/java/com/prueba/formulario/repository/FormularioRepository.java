package com.prueba.formulario.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.prueba.formulario.domain.Formulario;

public interface FormularioRepository extends JpaRepository<Formulario, Long> {
	List<Formulario> findAllByMail(@Param("mail") String mail);

    List<Formulario> findAll();
}
