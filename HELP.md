# CONFIGURACION DE INICIO PARA EL PROYECTO

Para este proyecto se debe tener previamente instalado y mapeado en el IDE (Eclipse idealmente) las dependencias de maven (la versión maven que se utilizó para este proyecto corresponde a la 3.6.3) además de las herramientas spring necesarias para levantar el proyecto.

Ubicar la carpeta del proyecto en la carpeta del workspace que se esté trabajando.

Importar el proyecto como proyecto maven en el ide y dejar que haga un build para descargar las dependencias necesarias, luego en la opción Run iniciar el proyecto como Spring Boot App.

Una vez levantado el proyecto se encontrará disponible en el puerto 8080.
